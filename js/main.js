'use strict'

/**
 * Теоретичне питання:
 * Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript
 *
 * Асинхронність - антонім синхронності :)
 * Тобто, асинхронне виконання - це послідовне виконання коду (перехід до наступного кроку відбувається лише після завершення попереднього.
 * Якщо інетрпретатору явно не вказати на необхідність асинхронного виконання коду (за допомогою async/await/then), він виконуватиме
 * код за замочанням - синхронно (тобто паралельно). Це означає, що перехід до виконання наступної інструкції буде здійснений без очікування
 * завершення попередньої.
 * */

async function getAddressByIp() {
    let IP = await fetch('https://api.ipify.org/?format=json')
    IP = await IP.json()
    let address = await fetch(`http://ip-api.com/json/${IP.ip}?fields=status,message,continent,country,regionName,city,district`)
    address = await address.json()

    let info = document.createElement('div');
    info.classList.add('info')
    info.innerHTML = `
                    <h2>Now I know where yor are and gonna kill you!!</h2>
                    <p>Continent: ${address.continent}</p>
                    <p>Country: ${address.country}</p>
                    <p>Region: ${address.regionName}</p>
                    <p>City: ${address.city}</p>
                    <p>District: ${address.district}</p>
                    `

    document.body.append(info);

}

document.querySelector('button').addEventListener('click', () => {
    document.querySelector('.info') ? document.querySelector('.info').remove() : false;
    getAddressByIp();
})
